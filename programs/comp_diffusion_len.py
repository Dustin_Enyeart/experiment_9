import math as ma
import numpy as np
import statistics as stat
import matplotlib.pyplot as plt
import scipy.optimize as optim


def comp_adjusted_counts():
    bg_bare = 221
    bg_covered = 4
    counts_bare = [309169, 120090, 46887, 19525, 8866]
    counts_covered = [5062, 1952, 779, 315, 168]
    counts_bare = [count - bg_bare for count in counts_bare]
    counts_covered = [count - bg_covered for count in counts_covered]
    adjusted_counts = [count_bare - count_covered
                       for count_bare, count_covered
                       in zip(counts_bare, counts_covered)]
    return adjusted_counts


def flux(r, L, C):
    return (C/r)*np.exp(-r/L)


def log_flux(r, L, C):
    return np.log(C) - np.log(r) - r/L


if __name__ == '__main__':

    distances = [15., 20., 25., 30., 35.]
    adjusted_counts = comp_adjusted_counts()
    print(adjusted_counts)

    log_adjusted_counts = [ma.log(count) for count in adjusted_counts]
    params = optim.curve_fit(log_flux, distances, log_adjusted_counts,
                             p0=[4, 100000])
    L = params[0][0]
    C = params[0][1]
    print(f'The diffusion length is {round(L, 2)} cm.')

    plt.scatter(distances, log_adjusted_counts, label='data')
    domain = np.linspace(15, 36, 100)
    computed_counts = [log_flux(r, L, C) for r in domain]
    plt.plot(domain, computed_counts, label='fit', color='red')
    plt.xlabel('distance (cm)')
    plt.ylabel('counts')
    plt.legend()
    plt.show()

    plt.scatter(distances, adjusted_counts, label='data')
    domain = np.linspace(15, 36, 100)
    computed_counts = [flux(r, L, C) for r in domain]
    plt.plot(domain, computed_counts, label='fit', color='red')
    plt.title('')
    plt.xlabel('distance (cm)')
    plt.ylabel('counts')
    plt.legend()
    plt.show()
