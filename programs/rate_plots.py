import matplotlib.pyplot as plt
import os


def plot(file, restricted=False):
    times = []
    rates = []
    with open(os.path.join('data', file)) as temp:
        for line in temp:
            time, rate = line.split()
            time = float(time)
            rate = float(rate)
            times.append(time)
            rates.append(2*rate)
    plt.plot(times, rates)
    plt.ylim(0)
    plt.xlabel('time (s)')
    plt.ylabel('rate (cps)')

    if (file == 'while_filling_tank.Asc'):
        noted_times = [100, 342, 504, 675]
        vals_at_times = [rates[2*time] for time in noted_times]
        annotations = ['filling started', 'reached lower line',
                       'reached center line', 'reached upper line']
        text_locations = [(0, 800), (235, 1150), (600, 150), (800, 2675)]
        for i in range(4):
            plt.annotate(annotations[i], xy=(noted_times[i], vals_at_times[i]),
                         xytext=text_locations[i],
                         arrowprops=dict({'facecolor':'black', 'width':.8,
                                          'headwidth':4., 'headlength':7.,
                                          'shrink':.05}))
    else:
        if 'prompt' in file:
            plt.title('Without water')
            plt.xlim(110, 320)
        elif 'delayed' in file:
            plt.title('With water')
            plt.xlim(80, 200)
        if restricted == False:
            plt.show()
            plot(file, restricted=True)
        else:
            plt.ylim(0, 150)


    #plt.savefig('plot.png')
    plt.show()

    return None


if __name__ == '__main__':
    files = ['while_filling_tank.Asc', 'delayed.Asc', 'only_prompt.Asc']
    for file in files:
        plot(file)
