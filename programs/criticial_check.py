import math as ma
import statistics as stat
import matplotlib.pyplot as plt
import numpy as np


def plot_line(i, factors):
    start = i
    a = factors[i]
    b = factors[i+1]
    domain = np.linspace(0, comp_intersection(list(range(start)) + [a, b]) + 1)
    image = [a + (b-a)*(x-start) for x in domain]
    plt.plot(domain, image, color='red')
    return None


def comp_intersection(factors):
    assert len(factors) >= 2
    return len(factors) - 2 + (factors[-2]/(factors[-2]-factors[-1]))



if __name__ == '__main__':
    reg = 51620
    counts = [55741, 59139, 62642, 68097, 70744, 73731, 75064, 76792, 81435]
    factors = [1] + [reg/count for count in counts]
    print(comp_intersection(factors))

    plt.plot(list(range(len(factors))), factors, marker='o')
    plt.ylim(0.)
    for i in range(len(factors)-1):
        plot_line(i, factors)

    plt.title('')
    plt.xlabel('step')
    plt.ylabel('inverse of multiplication factor')
    #plt.legend()
    #plt.savefig('plot.png')
    plt.show()
