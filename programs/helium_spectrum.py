import matplotlib.pyplot as plt

import lib

counts = lib.get_counts('he3_spectrum.Spe')
channels = list(range(0, len(counts)))
plt.plot(channels, counts, label='')
plt.ylim(0)
plt.xlabel('channel')
plt.ylabel('counts')
#plt.legend()
#plt.savefig('plot.png')
plt.show()
